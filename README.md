# wl55at

LoRaWAN AT-Kommandos für das STM32WL55-Nucleo oder verwandte Boards

Siehe die Anwendung "nucleo-wl55" in der TTN Konsole
https://eu1.cloud.thethings.network/console/applications/nucleo-wl55


```
(base)
hhoegl@x300:~ $ python wl55at.py -h
usage: wl55at.py [-h] [-s SERIAL] [-c] [-v] [--srv] [-j J] [-w WORKDIR]

options:
  -h, --help            show this help message and exit
  -s SERIAL, --serial SERIAL serial port (def. /dev/ttyACM0)
  -c, --cli             start cli
  -v, --version         print version info
  --srv                 run as server
  -j J                  join delay in seconds
  -w WORKDIR, --workdir WORKDIR path to ini- and log-file (def. /home/tsgrain/lora)
```

Bei lokalem Aufruf auf dem Entwicklungsrechner gibt man ein:

```
python wl55at.py -w . [weitere Optionen]
```

Damit wird das Arbeitsverzeichnis auf `./` gesetzt, so dass die `.ini` und die 
Logdatei `.log` im aktuellen Verzeichnis gesucht bzw. geschrieben wird. 

Falls beim Join in der TTN Konsole der Fehler "Join Server failed
DevNonce has already been used" gemeldet wird, dann muss man die DevNone
zurücksetzen. Das geht entweder in der TTN Konsole (End device
nucleo-stm32wl55-at-node1 -> Reiter "General Settings" -> "Join settings") 
oder von der Kommandozeile mit reset-dev-nonce.sh.


Die Logdatei `wl55at.log` sieht so aus:

```
[2023-03-23 16:29:58,431] INFO in wl55at: join_loop: finished
[2023-03-23 16:30:58,743] INFO in wl55at: send: payload=00 status=0
[2023-03-23 16:35:59,095] INFO in wl55at: send: payload=01 status=0
[2023-03-23 16:40:59,363] INFO in wl55at: send: payload=02 status=0
...
[2023-03-23 16:41:19,219] INFO in wl55at: sigterm_handler: received sigterm
```

Die sigterm Ausgabe erfolgt bei `sudo systemctl stop wl55at.service`.



