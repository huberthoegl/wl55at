'''

Wichtig: Wenn der WL55 einen Join gemacht hat und dann abgesteckt wurde,
dann klappt nach erneutem Einstecken der Join nur, wenn  in der TTN Konsole
unter General Settings / Join Settings der Knopf "Reset used DevNonces"
gedrueckt wurde. Am besten das im TTN CLI Tool machen (reset-dev-nonce.sh).

Ein paar AT Kommandos (siehe AN5481):
https://www.st.com/resource/en/application_note/an5481-lorawan-at-commands-for-stm32cubewl-stmicroelectronics.pdf

AT+DR=?     Abfrage Datenrate
AT+DR=<n>   Datenrate setzen (ADR muss disabled sein)

DR0 : SF12/BW125   250 bit/s
DR1 : SF11/BW125   440 bit/s
DR2 : SF10/BW125   980 bit/s
DR3 : SF9/BW125   1769 bit/s
DR4 : SF8/BW125   3125 bit/s
DR5 : SF7/BW125   5470 bit/s

Payload-Groesse abhaengig von DRx:
    DR0,1,2: 51 Byte
    DR3: 115 Byte
    DR4,5,6: 242 Byte


AT+ADR=?    ADR abfragen (0: disabled, 1: enabled)
AT+ADR=<n>  ADR setzen

AT+TXP=?    TX Power abfragen (0..7), 0 = max power +16dbm, 1 = +14dbm, ...
AT+TXP=<n>  TX Power setzen

AT+CLASS=?  Class abfragen (A, B, C)

AT+DCS=?    Duty cycle abfragen und setzen (0: disabled, 1: enabled)

Max Duty Cycle
0.1%  86 secs per day
1%    864 secs per day (ETSI recommendation)
10%   8640 secs per day

'''

# H. Hoegl, Hubert.Hoegl@t-online.de
# 2023-02-27

# flake8: noqa

import time
import threading
# https://code-maven.com/interactive-shell-with-cmd-in-python
from cmd import Cmd
# https://docs.python.org/3/howto/argparse.html
import argparse
import sys
import signal
import logging
from logging.config import dictConfig
import os

import serial  # type: ignore
from serial.threaded import ReaderThread, LineReader  # type: ignore

import configparser

__version = "0.2 2023-03-24"

SERIAL = '/dev/ttyACM0'

INIFILE = 'wl55at.ini'
WDIR = '/home/tsgrain/lora'  # RPi3

UNCONFIRMED = 0
CONFIRMED = 1

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--serial",
                    help="serial port (def. %s)" % SERIAL)
parser.add_argument("-c", "--cli", action='store_true', help="start cli")
parser.add_argument("-v", "--version", action='store_true', help="print version info")
parser.add_argument("--srv", action='store_true', help="run as server")
parser.add_argument("-j", help="join delay in seconds")
parser.add_argument("-w", "--workdir", help="path to ini- and log-file (def. %s)" % WDIR)

args = parser.parse_args()

inifile = configparser.ConfigParser()
if args.workdir:
    inipath = os.path.join(args.workdir, INIFILE)
else:
    inipath = os.path.join(WDIR, INIFILE)

if not os.path.exists(inipath):
    print("inifile does not exist: {} - exit program".format(inipath))
    sys.exit(1)
else:
    inifile.read(inipath)

if args.workdir:
    logfile = os.path.join(args.workdir, inifile['generic']['logfile'])
else:
    logfile = os.path.join(WDIR, inifile['generic']['logfile'])


# try next join in xx seconds
NEXT_JOIN_DELAY = int(args.j) if args.j else int(inifile['generic']['join_delay'])

port = args.serial if args.serial else SERIAL

loggername = inifile['generic']['logger']


dictConfig({
    'version': 1,
    'formatters': {
        'default': {
           'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
        }
    },
    'handlers': {
        'file': {
            'class': 'logging.FileHandler',
            'level': 'INFO',
            'formatter': 'default',
            'filename': logfile
        },
        'systemd': {
            'class': 'logging.StreamHandler',
            'level': 'INFO',
            # 'stream': sys.stderr,   # default sys.stderr
            'formatter': 'default'
        }
    },
    'root': {   # root logger
        'level': 'INFO',
        'handlers': ['file', 'systemd']
    }
})


logger = logging.getLogger(loggername)

try:
    serial_port = serial.Serial(port, 9600, timeout=1.0)
    logger.info("opened serial port {}".format(port))
except serial.SerialException as e:
    logger.info("can not open serial port {} - exit program".format(port))
    sys.exit(1)

lines = []
lock = threading.Lock()


def sigterm_handler(signal, frame):
    # Called when server is stopped with "systemctl stop srv".
    logger.info("sigterm_handler: received sigterm")
    sys.exit(0)



def int_to_hex(number):
    # stackoverflow.com
    hrepr = hex(number).replace('0x', '')
    if len(hrepr) % 2 == 1:
        hrepr = '0' + hrepr
    return hrepr


# https://stackoverflow.com/questions/70625801/threading-reading-a-serial-port-in-python-with-a-gui
class SerialReaderProtocolLine(LineReader):
    # TERMINATOR = b'\n\r'
    TERMINATOR = b'\n'

    def __init__(self, *args, **kwargs):
        super(SerialReaderProtocolLine, self).__init__(*args, **kwargs)

    def connection_made(self, transport):
        """Called when reader thread is started"""
        super().connection_made(transport)
        print("Connected, ready to receive data...")

    def handle_line(self, line):
        '''
        line ends with \r
        '''
        global lines, lock
        """New line waiting to be processed"""
        line = line.rstrip()
        if line == '':  # or line.startswith('#####'):
            pass
        else:
            print(": %s" % line.rstrip())
            lock.acquire()
            lines.append(line.rstrip())
            lock.release()

    def connection_lost(self, exc):
        print("connection lost:", exc)


def in_list_start(L, S):
    '''Check if one or more of the strings in L starts with one of the strings
    in S
    '''
    M = []
    for s in S:
        R = map(lambda t: t.startswith(s), L)
        M.append(any(R))
    return any(M)

def in_list_find(L, S):
    '''Check if one or more of the strings in L contains one of the strings
    in S
    '''
    M = []
    for s in S:
        R = map(lambda t: t.find(s) >= 0, L)
        M.append(any(R))
    return any(M)


def wait_for(evt_ok=[], evt_err=[]):
    # for all commands:
    # success: OK
    # error: AT_ERROR, AT_PARAM_ERROR, AT_BUSY_ERROR, AT_TEST_PARAM_OVERFLOW,
    #        AT_NO_NETWORK_JOINED, AT_RX_ERROR
    # some commands have additional events for ok/err, e.g. +EVT:JOINED or
    # +EVT:JOIN FAILED.
    # returns 0 on success, 1 on failure, 2 on timeout
    global lines, lock
    n = 0
    while n < 60:  # timeout 60 * 0.25 sec = 15 sec
        if len(lines) > 0:
            if not evt_ok and in_list_start(lines, ["OK"]):
                # print("wait_for is ok -> 1")
                return 0
            elif in_list_start(lines, ["AT_ERROR", "AT_RX_ERROR"]):
                # print("wait_for is failure -> 0")
                return 1
            elif evt_ok and in_list_find(lines, evt_ok):
                # print("wait_for is ok -> 1")
                return 0
            elif evt_err and in_list_find(lines, evt_err):
                # print("wait_for is failure -> 0")
                return 1
        time.sleep(0.25)
        n += 1
    # print("wait_for is timeout -> 2")
    return 2


def send_low(s, comment=''):
    global lines, lock
    print(f"sending {s} ... [{comment}]")
    lock.acquire()
    lines = []
    lock.release()
    serial_port.write(f"{s}\n".encode("utf-8"))



def join():
    '''return 0 if ok, 1 if error, 2 on timeout'''
    send_low("AT+JOIN=1")
    r = wait_for(evt_ok=["+EVT:JOINED"], evt_err=["+EVT:JOIN FAILED"])
    return r


def join_loop():
    '''Periodically try to join() until device is joined.

    If the device was plugged off and on, this function will not exit until the
    DevNone has been reset in the TTN Console (Device -> General Settings ->
    Join Settings -> Reset used DevNonces)

    return 0 if ok, 1 if error, 2 on timeout
    '''
    while True:
        r = join()
        if r == 0:
            break
        else:
            time.sleep(NEXT_JOIN_DELAY)
            logger.info("join_loop: trying next join")
    logger.info("join_loop: finished")
    return 0


def send(port, mode, payload):
    '''send unconfirmed or confirmed

    mode is 0 (unconfirmed) or 1 (confirmed)
    payload is a hex str with an even number of digits
    return 0 if ok, 1 if error.
    '''
    send_low(f"AT+SEND={port}:{mode}:{payload}")
    if mode == UNCONFIRMED:
        r = wait_for(evt_err = ['AT_PARAM_ERROR', 'AT_NO_NETWORK_JOINED'])
    else:
        # evt_err?
        r = wait_for(evt_ok=["+EVT:SEND_CONFIRMED"])
    return r


if args.cli or args.srv:
    reader = ReaderThread(serial_port, SerialReaderProtocolLine)
    reader.start()


class MyPrompt(Cmd):
    prompt = 'wl55> '
    intro = """Welcome! Type ? to list commands. If Prompt disappears type Return."""

    def default(self, inp):
        if inp == 'x' or inp == 'q':
            return self.do_exit(inp)

    def do_quit(self, inp):
        '''exit program'''
        print("Bye")
        return True

    def do_exit(self, inp):
        '''exit program'''
        print("Bye")
        return True

    def help_exit(self):
        print('exit the application. Shorthand: x q Ctrl-D.')

    def do_at(self, inp):
        '''AT'''
        send_low("AT")
        r = wait_for()
        print(r)

    def do_bat(self, inp):
        '''AT+BAT=?'''
        send_low("AT+BAT=?")
        r = wait_for()
        print(r)

    def do_join(self, inp):
        '''AT+JOIN=1 (OTAA)'''
        r = join()
        print(r)

    def do_send(self, inp):
        '''send unconfirmed message

        send [payload]
        '''
        port = 2
        mode = UNCONFIRMED
        payload = inp if inp else "01234567"
        r = send(port, mode, payload)
        print(r)

    def do_sendc(self, inp):
        '''send confirmed message

        sendc [payload]
        '''
        port = 2
        mode = CONFIRMED
        payload = inp if inp else "76543210"
        r = send(port, mode, payload)
        print(r)

    def do_raw(self, inp):
        '''Send raw AT command

Example:
raw AT+ADR=?
        '''
        send_low(inp)
        r = wait_for()
        print(r)

    def do_vers(self, inp):
        '''show version'''
        send_low('AT+VER=?')
        r = wait_for()
        print(r)


    def emptyline(self):
        # do not repeat last command on newline
        pass

    do_EOF = do_exit
    help_EOF = help_exit


def initial_commands():
    # ADR off, max TXP, DCS off
    cmds = """AT
AT
AT+ADR=0
AT+TXP=0
AT+DCS=0
"""
    lines = cmds.splitlines()
    for line in lines:
        send_low(line)
        r = wait_for()
        print(r)


if __name__ == "__main__":
    # print(in_list_start(["abc", "def  ", "gjijk", "abc"], "abc"))
    # print(in_list_find(["abcdefg", "qrstuvw", "ijklmnop"], "qk"))
    signal.signal(signal.SIGTERM, sigterm_handler)
    if args.version:
        print(__version)
        sys.exit(0)
    if args.cli or args.srv:
        initial_commands()
    if args.cli:
        MyPrompt().cmdloop()
    if args.srv:
        join_loop()
        time.sleep(1 * 60)
        port = 2
        i = 0
        while True:
            payload = int_to_hex(i)  # must have even number of digits
            r = send(port, UNCONFIRMED, payload)
            if r == 0:
                logger.info("send: payload={} status={}".format(payload, r))
                time.sleep(15 * 60)
                i = i + 1
            else:
                # should not happen...
                logger.info("send: no success - trying join_loop")
                join_loop()
                time.sleep(1 * 60)

